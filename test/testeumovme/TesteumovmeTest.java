/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testeumovme;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rafaeldasilvadick
 */
public class TesteumovmeTest {
    
    private CalcDesc calculadora;
    
    public TesteumovmeTest() {
        calculadora = new CalcDesc();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
  
    /*
        Teste o valor dos descontos para Segunda-Feira
    */
    @Test
    public void testSegCri(){
        Pessoa p1 = new Pessoa("Criança");
        assertEquals(calculadora.getValorDesconto(p1, 1), 4.95,1);
    }
    @Test
    public void testSegEst(){
        Pessoa p1 = new Pessoa("Estudante");
        assertEquals(calculadora.getValorDesconto(p1, 1), 8,1);
    }
    @Test
    public void testSegIdo(){
        Pessoa p1 = new Pessoa("Idoso");
        assertEquals(calculadora.getValorDesconto(p1, 1), 6,1);
    }
    
    
    /*
        Teste descontos de Terça-Feira
    */
    
     @Test
    public void testTerCri(){
        Pessoa p1 = new Pessoa("Criança");
        assertEquals(calculadora.getValorDesconto(p1, 2), 4.67, 1);
    }
    @Test
    public void testTerEst(){
        Pessoa p1 = new Pessoa("Estudante");
        assertEquals(calculadora.getValorDesconto(p1, 2), 7.6, 1);
    }
    @Test
    public void testTerIdo(){
        Pessoa p1 = new Pessoa("Idoso");
        assertEquals(calculadora.getValorDesconto(p1, 2), 5.1, 1);
    }
    
    /*
        Teste descontos de Quarta-Feira
    */
    
     @Test
    public void testQuaCri(){
        Pessoa p1 = new Pessoa("Criança");
        assertEquals(calculadora.getValorDesconto(p1, 3), 3.85, 1);
    }
    @Test
    public void testQuaEst(){
        Pessoa p1 = new Pessoa("Estudante");
        assertEquals(calculadora.getValorDesconto(p1, 3), 4, 1);
    }
    @Test
    public void testQuaIdo(){
        Pessoa p1 = new Pessoa("Idoso");
        assertEquals(calculadora.getValorDesconto(p1, 3), 3.6, 1);
    }
    
    /*
        Teste descontos de Quinta-Feira
    */
    
     @Test
    public void testQuiCri(){
        Pessoa p1 = new Pessoa("Criança");
        assertEquals(calculadora.getValorDesconto(p1, 4), 0, 1);
    }
    @Test
    public void testQuiEst(){
        Pessoa p1 = new Pessoa("Estudante");
        assertEquals(calculadora.getValorDesconto(p1, 4), 5.6, 1);
    }
    @Test
    public void testQuiIdo(){
        Pessoa p1 = new Pessoa("Idoso");
        assertEquals(calculadora.getValorDesconto(p1, 4), 4.2, 1);
    }
    
     /*
        Teste descontos de Sexta-Feira
    */
    
     @Test
    public void testSexCri(){
        Pessoa p1 = new Pessoa("Criança");
        assertEquals(calculadora.getValorDesconto(p1, 5), 4.45, 1);
    }
    @Test
    public void testSexEst(){
        Pessoa p1 = new Pessoa("Estudante");
        assertEquals(calculadora.getValorDesconto(p1, 5), 0, 1);
    }
    @Test
    public void testSexIdo(){
        Pessoa p1 = new Pessoa("Idoso");
        assertEquals(calculadora.getValorDesconto(p1, 5), 0, 1);
    }
    
    /*
        Teste descontos de Sexta-Feira
    */
    
     @Test
    public void testSDSFCri(){
        Pessoa p1 = new Pessoa("Criança");
        assertEquals(calculadora.getValorDesconto(p1, 6), 0, 1);
    }
    @Test
    public void testDSFEst(){
        Pessoa p1 = new Pessoa("Estudante");
        assertEquals(calculadora.getValorDesconto(p1, 6), 0, 1);
    }
    @Test
    public void testDSFIdo(){
        Pessoa p1 = new Pessoa("Idoso");
        assertEquals(calculadora.getValorDesconto(p1, 6), 5.7, 1);
    }
}
