/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testeumovme;

import java.util.ArrayList;
public class Testeumovme {

    static final int seg = 1;
    static final int ter = 2;
    static final int qua = 3;
    static final int qui = 4;
    static final int sex = 5;
    static final int dsf = 6;
    
    private static ArrayList<Pessoa> array;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        /* 
            Valor dos ingressos:
                - Crianças: R$ 5.50
                - Estudantes: R$ 8.00
                - Idosos: R$ 6.00
        */
        
        
        executar();
        
    }
    
    
    public static void executar(){
        System.out.println("Calulo de descontos");
       
        array = new ArrayList<>();
        
        Pessoa crianca = new Pessoa("Criança");  
        array.add(crianca);
 
        Pessoa idoso = new Pessoa("Idoso");  
        array.add(idoso);
         
        Pessoa estudante = new Pessoa("Estudante");   
        array.add(estudante);
        
        CalcDesc calculadora = new CalcDesc();
        
       for( int k=0; k<array.size(); k++){
           System.out.println("");
           if(k==0){
               System.out.println("Descontos para Crianças");
           } else if(k==1){
               System.out.println("Descontos para Idosos");
           }else{
                System.out.println("Descontos para Estudantes");
           }
           for(int i=1; i<=6; i++ )
            calculadora.getValorDesconto(array.get(k), i);
       }
    }
    
}
