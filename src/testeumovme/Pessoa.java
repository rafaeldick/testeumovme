/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testeumovme;


/**
 *
 * @author rafaeldasilvadick
 */


public class Pessoa {
    
    static final String cri = "Criança";
    static final String est = "Estudante";
    static final String ido = "Idoso";
    
    private final String tipoPessoa;
    private final double valor;
    private boolean isCrianca = false;
    private boolean isEstudante = false;
    private boolean isIdoso = false;

    public Pessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
        
        if (tipoPessoa.equalsIgnoreCase(cri)) {
           this.valor = 5.5;
           this.isCrianca = true;
        } else if (tipoPessoa.equalsIgnoreCase(est)) {
          this.valor = 8;
          this.isEstudante = true;
        } else {
            this.valor = 6;
            this.isIdoso = true;
        }
    }

    public boolean isIsCrianca() {
        return isCrianca;
    }

    public boolean isIsEstudante() {
        return isEstudante;
    }

    public boolean isIsIdoso() {
        return isIdoso;
    }

    public String getTipo() {
        return tipoPessoa;
    }

    public double getValor() {
        return valor;
    }
    
}
