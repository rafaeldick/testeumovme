/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testeumovme;

/**
 *
 * @author rafaeldasilvadick
 */
public class CalcDesc {
    
    private final double descSeg = 0.1;
    private final double descTerIdoCri = 0.15;
    private final double descTerEst = 0.05;
    private final double descQuaIdo = 0.4;
    private final double descQuaCri = 0.3;
    private final double descQuaEst = 0.5;
    private final double descQuiIdoEst = 0.3;
    private final double descSexCri = 0.11;
    private final double descDFSIdo = 0.05;
    
    
    public double getValorDesconto(Pessoa pessoa, int diaSemana){
        
        double preco = pessoa.getValor();
        double valorDesconto = 0;
        
        switch (diaSemana){
            case 1:
                valorDesconto = preco - (preco * descSeg);
                System.out.println("Valor do desconto Segunda:" + String.valueOf(valorDesconto));
                break;
            
            case 2:
                if(pessoa.isIsCrianca() || pessoa.isIsIdoso()){
                    valorDesconto = preco - (preco * descTerIdoCri);
                    System.out.println("Valor do desconto Terça-Feira para crianças ou idosos:" + String.valueOf(valorDesconto));

                } else{
                    valorDesconto = preco - (preco * descTerEst);
                    System.out.println("Valor do desconto Terça-Feira para estudantes:" + String.valueOf(valorDesconto));
                }
                break;
            
            case 3:
                if(pessoa.isIsCrianca()){
                    valorDesconto = preco - (preco * descQuaCri);
                    System.out.println("Valor do desconto Quarta-Feira para crianças:" + String.valueOf(valorDesconto));
                } else if(pessoa.isIsEstudante()){
                    valorDesconto = preco - (preco * descQuaEst);
                    System.out.println("Valor do desconto Quarta-Feira para estudantes:" + String.valueOf(valorDesconto));
                } else{
                    valorDesconto = preco - (preco * descQuaIdo);
                    System.out.println("Valor do desconto Quarta-Feira para idosos:" + String.valueOf(valorDesconto));
                }
                break;
                
            case 4:
                if(pessoa.isIsEstudante() || pessoa.isIsIdoso()){
                    valorDesconto = preco - (preco * descQuiIdoEst);
                    System.out.println("Valor do desconto Quinta-Feira para estudantes ou idosos:" + String.valueOf(valorDesconto));
                }
                break;
                
            case 5:
                if(pessoa.isIsCrianca()){
                    valorDesconto = preco - (preco * descSexCri);
                    System.out.println("Valor do desconto Sexta-Feira para crianças:" + String.valueOf(valorDesconto));
                }
                break;
            
            case 6:
                if(pessoa.isIsIdoso()){
                    valorDesconto = preco - (preco * descDFSIdo);
                    System.out.println("Valor do desconto para Sábados/Domingos/Feriados para idosos:" + String.valueOf(valorDesconto));
                }
                break;
            default:
                break;
        }
        return valorDesconto;
    }
    
}
